function factorial(n) {
  // if(typeof n !== 'number') return undefined; // will use in s04
  // if(n<0) return undefined; // will use in s04
  if (n <= 1) return 1;
  return n * factorial(n - 1);
}
//activity
function div_check(n) {
  return n % 5 === 0 || n % 7 === 0;
}
module.exports = {
  factorial,
  div_check,
};
