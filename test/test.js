const { assert, expect } = require("chai");
const { factorial } = require("../src/util.js");

describe(`(1) TEST FUNCTION FACOTRIALS`, () => {
  const FACTORIAL_OF_0 = factorial(0);
  const FACTORIAL_OF_1 = factorial(1);
  const FACTORIAL_OF_4 = factorial(4);
  const FACTORIAL_OF_5 = factorial(5);
  const FACTORIAL_OF_10 = factorial(10);
  it(`test_fun_factorial_0!_is_${FACTORIAL_OF_0}`, () => {
    const product = factorial(0);
    assert.equal(product, FACTORIAL_OF_0);
  });
  it(`test_fun_factorial_1!_is_${FACTORIAL_OF_1}`, () => {
    const product = factorial(1);
    assert.equal(product, FACTORIAL_OF_1);
  });
  it(`test_fun_factorial_4!_is_${FACTORIAL_OF_4}`, () => {
    const product = factorial(4);
    assert.equal(product, FACTORIAL_OF_4);
  });
  it(`test_fun_factorial_5!_is_${FACTORIAL_OF_5}`, () => {
    const product = factorial(5);
    expect(product).to.equal(FACTORIAL_OF_5);
  });
  it(`test_fun_factorial_10!_is_${FACTORIAL_OF_10}`, () => {
    const product = factorial(10);
    assert.equal(product, FACTORIAL_OF_10);
  });
});
describe(`(1) TEST FUNCTION DIVISIBILITY BY 5 or 7`, () => {
  const TEST_CASES = [100, 49, 30, 56];
  TEST_CASES.forEach((testCase) => {
    const test = div_check(testCase);
    it(`test_${testCase}_is_divisible_by_5_or_7`, () => {
      const isDivisible = div_check(testCase);
      assert.equal(isDivisible, test);
    });
  });
});
